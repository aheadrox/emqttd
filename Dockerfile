FROM ubuntu:latest
MAINTAINER Denis Grigoryev <dgrigoryev@maprox.net>

# RUN mkdir -p /opt/emqttd

ADD http://emqtt.io/downloads/latest/ubuntu /emqttd-ubuntu64.zip

RUN apt-get -qq update && apt-get install -y libssl1.0.0 unzip iputils-ping && unzip /emqttd-ubuntu64.zip -d /opt/ && \
    rm /emqttd-ubuntu64.zip && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    mkdir /opt/emqttd/defaults && cp -avr /opt/emqttd/etc /opt/emqttd/defaults && cp -av /opt/emqttd/releases/2.0/vm.args /opt/emqttd/defaults/etc

COPY ./start.sh /opt/emqttd
# COPY ./emqttd /opt/emqttd/defaults
WORKDIR /opt/emqttd
RUN chmod +x ./start.sh
EXPOSE 1883 8883 8083 18083
# VOLUME ["/opt/emqttd/etc", "/opt/emqttd/data", "/opt/emqttd/plugins"]

#CMD [ "/bin/bash" ]
CMD [ "./start.sh"]

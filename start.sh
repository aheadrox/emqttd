#!/bin/bash

#NODE_IP=`hostname`
NODE_NAME=`hostname`.$NODE_DOMAIN

echo $NODE_NAME hostname.
cp -avnr ./defaults/etc ./
cp -f ./defaults/etc/vm.args ./releases/2.0/
sed -i 's/127.0.0.1/'"$NODE_NAME"'/g' ./releases/2.0/vm.args
sed -i 's/emqttdsecretcookie/'"$COOKIE"'/g' ./releases/2.0/vm.args
./bin/emqttd start
sleep 5
if [ -z ${MASTER+x} ]; then
echo '$MASTER' is not set.
else
./bin/emqttd_ctl cluster emqttd@$MASTER;
fi
sleep 10 && tail -f --retry /opt/emqttd/log/*
